Octree is a 3D analog to 1D binary tree and 2D quadtree. Each node in an octree
has a maximum of eight children.

A tree based implementation~\citep{Katz} can be used to efficiently find neighbors in cases of
variable support radius.

\subsection*{Building the tree}

For every node in the tree, the maximum $h$ appearing in that node is stored for 
finding symmetric neighbors~\citep{Gadget2}.

\subsection*{Nearest neighbor querying}

Define $S(q,r)$ as the bounding box for the query point $q$ and radius $r$.
Recursively traverse the tree.
Consider an octant $O$ with maximum support radius $\hat{h}_{O}$.
\[ r = \max(h_q, \hat{h}_{O}) \]

If the regions $S(q,r)$ and $O$ overlap, recursively query for neighbors on
children of $O$.
If $O$ is a leaf node, check for nearest neighbors in $O$.

\subsection*{Overlapping regions}

The following method can be used to find whether two regions overlap. \\

Let $d : \mathbb{S} \to \mathbb{R}$ where $\mathbb{S}$ is a set of bounding boxes and
$\mathbb{R}$ the set of real numbers map a bounding box to it's side length. \\

Consider two regions $S, R \in \mathbb{S}$.

Let $\mathbf{s} = (s_1, s_2, s_3)$ be centre of region $S$ and $\mathbf{r} = (r_1, r_2, r_3)$
be the centre of the region $R$.

Then, the two regions overlap if,
\[ |r_i - s_i| < \frac{d(R) + d(S)}{2} \ \ \forall \ \ i \in \{1,2,3\} \]

\noindent{\textbf{Proof:}} \\

Assume wlog that $d(R) \leq d(S)$.

Define a co-ordinate transform, such that,
\begin{align*}
r'_i &= |r_i - s_i| \\
s'_i &= 0
\end{align*}

Define a new region $R'$ with $d(R') = d(R)$ and centre of the region at $\mathbf{r}' = (r'_1, r'_2, r'_3)$.
Also define a new region $S'$ such that $d(S') = d(S)$ centered at $\mathbf{s}' = (0,0,0)$.

Note that $||\mathbf{r} - \mathbf{s}|| = ||\mathbf{r}' - \mathbf{s}'||$. Thus if the regions $R'$ and
$S'$ overlap, the regions $R$ and $S$ also overlap.

Note that the point in region $R'$ farthest from $\mathbf{r}'$ towards $\mathbf{s}'$ is,
\[ r^{\min}_i = r'_i - \frac{d(R')}{2} \]

The point in region $S'$ farthest from $\mathbf{s}'$ towards $\mathbf{r}'$ is,
\[ s^{\max}_i = \frac{d(S')}{2} \]

Observe that because $R'$ is smaller than $S'$ and $\mathbf{r}'$ is always in the first quadrant,
the two regions overlap only if the point in region $R'$ farthest from $\mathbf{r}'$ is
inside the region $S'$, ie. $r^{\min}_i < s^{\max}_i$,
\begin{align*}
&\implies \ r'_i < \frac{d(R') + d(S')}{2} \\
&\implies \ |r_i - s_i| < \frac{d(R) + d(S)}{2}
\end{align*}

Note that we get the same expression for $d(R) > d(S)$. Thus, the sizes of the boxes need not be checked
while implementing.

\subsection*{Handling floating point error}

When a particle is very close to the edge of an octant, it gets mapped to the wrong cell because of
a floating point error. Consider an octant of length $L_m$ (at level $m$), the child octant is calculated as follows,
\[ i = \floor*{\frac{x - x^m_{\min}}{L_m/2}} \]
Consider $x = x^m_{\min} + \frac{nL_m}{2} - \delta$,
\begin{align*}
\implies i  &= \floor*{n - \frac{2\delta}{L_m}} \\
            &= \floor*{n\left( 1 - \frac{2\delta}{nL_m} \right)}
\end{align*}

If $\frac{2\delta}{nL_m} < \mathbf{u}$ where $\mathbf{u}$ is the machine epsilon, the point will be mapped
to $i = n$ when it should be mapped to $i = n-1$. (Observe that $1 \leq n \leq 2$)

The following method is used to find domain of the children, for an octant $(i,j,k)$,
the \\
$(x^m_{\min}, y^m_{\min}, z^m_{\min})$ for the octant (level $m$) is given by,
\[ x^m_{\min} = x_{\min}^{m-1} + \frac{iL_{m-1}}{2} \]
\[ y^m_{\min} = y_{\min}^{m-1} + \frac{jL_{m-1}}{2} \]
\[ z^m_{\min} = z_{\min}^{m-1} + \frac{kL_{m-1}}{2} \]

where $(x_{\min}^{m-1}, y_{\min}^{m-1}, z_{\min}^{m-1})$ are the minimum values of parent (level $m-1$).
Observe that in the case discussed above, $x_{\min}^{m+1} = x_{\min}^{m} + \frac{nL_{m}}{2}$. Thus,
$x < x_{\min}^{m+1}$.
The particle will be mapped to the wrong cell and in the next level will leak out of the tree.

Observe that the problem can be solved if the bounds of the children are expanded to account for these
floating point errors, ie. if we add a padding to the $x^m_{\min}$ and $L_m$ larger than the machine epsilon.

To fix this problem, we define a new way to find $(x^m_{\min}, y^m_{\min}, z^m_{\min})$ as follows,
\[ x^m_{\min} = x_{\min}^{m-1} + (i - \epsilon_{m-1})\frac{L_{m-1}}{2} \]
\[ y^m_{\min} = y_{\min}^{m-1} + (j - \epsilon_{m-1})\frac{L_{m-1}}{2} \]
\[ z^m_{\min} = z_{\min}^{m-1} + (k - \epsilon_{m-1})\frac{L_{m-1}}{2} \]
\[ L_m = \frac{L_{m-1}}{2}(1 + 2\epsilon_{m-1}) \]
\[ x_{\min}^{0} = x_{\min} - L\epsilon \]
\[ L_0 = L(1 + 2\epsilon) \]

where $x_{\min}$ is the actual minimum value of $x$ for all the particles, $L = x_{\max} - x_{\min}$
and $\epsilon_{m}$ and $\epsilon$ are small numbers such that, 
\[ \epsilon > \frac{\mathbf{u}}{L}\max{(\abs*{\mathbf{x}_{\min}}, L)} \]
\[ \epsilon_{m} > \frac{2\mathbf{u}}{L_m}\max{(\abs*{\mathbf{x}^m_{\min}},{L_m})} \]
where,
\[ \abs*{\mathbf{x}_{\min}} = \max{(\abs*{x_{\min}}, \abs*{y_{\min}}, \abs*{z_{\min}})} \] 
\[ \abs*{\mathbf{x}^0_{\min}} = \max{(\abs*{x^0_{\min}}, \abs*{y^0_{\min}}, \abs*{z^0_{\min}})} \]

\noindent{\textbf{Proof:}} \\

Note that the problem we need to deal with is not that the particle gets mapped to the wrong cell
but the fact that the particle leaks out of the tree in the next level if the node is not padded.

For the example shown above, $x = x_{\min}^{m} + \frac{nL_{m}}{2} - \delta$, $x^{m+1}_{\min}$ using the
modified definition would be,
\[ x^{m+1}_{\min} = x_{\min}^{m} + \frac{nL_{m}}{2} - \frac{L_{m}\epsilon_{m}}{2} \]

Observe that $x < x^{m+1}_{\min}$ only if $\delta > \frac{L_{m}\epsilon_{m}}{2}$. As $\epsilon_{m} > 2\mathbf{u}$,
the error only happens when $\delta > L_{m}\mathbf{u}$. But as stated above the wrong mapping
of particle happens only when $\delta < \frac{nL_{m}\mathbf{u}}{2}$ ie. when $\delta < L_{m}\mathbf{u}$ as $n \leq 2$.

We said earlier that wrong mapping of particle is not a problem as long as the $x_{\min}$ and $L$
are adjusted. However, observe that the wrong mapping can infact be a problem if the particle gets
mapped outside the eight child octants. For example consider the case mentioned above with $n = 2$,
ie. $x = x^m_{\min} + L_m - \delta$. Note that the particle will be wrongly mapped to $i = 2$
when $\delta < L_m\mathbf{u}$.
As the above mentioned corrections do not fix the wrong mapping of particles, the above situation
will cause a problem that won't be fixed by the proposed method.

The proposed method does however solve this problem indirectly by not allowing this situation
to ever occur. Note that the above value of $x$ can also be written as $x = x_{\max} - \delta$.
The proposed corrections pad each octant by a small value (say $\Delta$) such that $\Delta > 2L_m\mathbf{u}$.
Thus the space around the octant of thickness $L_m\mathbf{u}$ will never have any particle.

For the same reason mentioned above, we need a padding to the original domain itself.
Consider a case when $x = x_{\min} + L$. In this case, the point will leak out of the tree straight away
if the domain is not padded. Thus, we define an $\epsilon$ such that $x^0_{\min} = x_{\min} - L\epsilon$
and $L_0 = L(1 + 2\epsilon)$.
Thus, while calculating for level $1$, 
\begin{align*}
i &= \floor*{\frac{L + L\epsilon}{\frac{L}{2}\left(1 + 2\epsilon\right)}} \\
&= \floor*{\frac{2 + 2\epsilon}{1 + 2\epsilon}} \\
&= 1
\end{align*}
From above expressions, if $\epsilon > \mathbf{u}$ and $\epsilon > \frac{\mathbf{u}\abs*{x_{\min}}}{L}$,
wrongly mapped particles will never leak out of the tree because of calculation for $(i, j, k)$.

However, note that a floating point can still occur while calculating $x^{m+1}_{\min}$,
\[ x^{m+1}_{\min} = x_{\min}^{m} + (i - \epsilon_{m})\frac{L_{m}}{2} \]

Observe that floating point error cannot occur when $i = 1$. For $i = 0$,
\[ x^{m+1}_{\min} = x_{\min}^{m} - \frac{\epsilon_{m}L_{m}}{2} \]
Thus,
\[ x^{m+1}_{\min} = x_{\min}^{m} \left( 1 - \frac{\epsilon_{m}L_{m}}{2x_{\min}^{m}} \right) \]
\[ \epsilon_m > \abs*{\frac{2\mathbf{u}x_{\min}^{m}}{L_m}} \]
We get similar results for other two dimensions. Thus,
\[ \epsilon_m > \frac{2\mathbf{u}\abs*{\mathbf{x}_{\min}^{m}}}{L_m} \]

Note that because of the padding in length, some regions can potentially be checked for neighbors 
unnecessarily. To avoid this, choose $\epsilon$ and $\epsilon_m$ to be as small as possible.

\subsection*{Compressed Octree}

Consider a dataset as shown in figure~\ref{fig:pat_data}. Now, if an octree is constructed on this
dataset using algorithm~\ref{build_tree_1}, the tree has a large number of redundant branches
(see figure~\ref{fig:pat_octree}). This added depth results in larger memory requirement and
an unnecessary overhead while traversing the tree. Both of these problems can be solved by
recalculating the domain at every child node instead of just dividing the parent into eight
equal parts.

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/octree/pathological.eps}
\caption{}
\label{fig:pat_data}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/octree/pathological_tree.eps}
\caption{}
\label{fig:pat_tree}
\end{subfigure}

\caption{Figure~\subref{fig:pat_tree} is an octree constructed on a dataset in
figure~\subref{fig:pat_data}}
\label{fig:pat_octree}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/chapters/octree/pathological_comp_tree.eps}
\caption{Compressed octree built using algorithm~\ref{build_tree_2}}
\label{fig:comp_tree}
\end{figure}

\subsubsection*{Handling floating point error}

Note that as is in the case of a normal octree, a compressed octree is also susceptible to floating
point errors.

To avoid floating point errors, the bounds in the case of a compressed octree are calculated as follows,
\[ x^m_{\min} = \bar{x}^m_{\min} - \bar{L}_m\epsilon_m \]
\[ y^m_{\min} = \bar{y}^m_{\min} - \bar{L}_m\epsilon_m \]
\[ z^m_{\min} = \bar{z}^m_{\min} - \bar{L}_m\epsilon_m \]
\[ L_m = \bar{L}_m(1 + 2\epsilon_m) \]
\[ \epsilon_m > \frac{\abs*{\bar{\mathbf{x}}^m_{\min}}\mathbf{u}}{L_m} \]
where $(\bar{x}^m_{\min}, \bar{y}^m_{\min}, \bar{z}^m_{\min})$ are the minimum values of $x, y$ and 
$z$ in an octant at level $m$ and $(x^m_{\min}, y^m_{\min}, z^m_{\min})$ are the new padded minimum 
values and $L_m$ is the new padded length. \\

\noindent{\textbf{Proof:}} \\

Consider an octant at level $m$. Let $x = x^m_{\min} + \frac{nL_m}{2} - \delta$ be a point in
space. Similar to the normal octree,
\begin{align*}
i &= \floor*{\frac{x - x^m_{\min}}{L_m/2}} \\
\implies i &= \floor*{n - \frac{2\delta}{L_m}}
\end{align*}
Thus, the particle, similar to the octree, will be mapped to the wrong cell.

However, note that the floating point error in octree was being caused not because of the wrong mapping
of the particle but because the value of $x^{m+1}_{\min}$ calculated turned out to be larger than
$x$.

In the case of compressed octree, $\bar{x}^{m+1}_{\min}$ is the minimum value of $x$ for all particles
in the child octant. Thus, $\bar{x}^{m+1}_{\min} \leq x \implies x^{m+1}_{\min} < x$. Thus, wrong mapping
of particle does not cause leaking out of the particle.

However, error can occur while calculating $x^{m+1}_{\min} = \bar{x}^{m+1}_{\min} - \bar{L}_{m+1}\epsilon_{m+1}$
which could lead to a corner case where $x^{m+1}_{\min} = x$ which will cause problems a level below.
\begin{align*}
x^{m+1}_{\min} &= \bar{x}^{m+1}_{\min} \left(1 - \frac{\bar{L}_{m+1}\epsilon_{m+1}}{\bar{x}^{m+1}_{\min}}\right) \\
&\implies \abs*{\frac{\bar{L}_{m+1}\epsilon_{m+1}}{\bar{x}^{m+1}_{\min}}} > \mathbf{u} \\
&\implies \epsilon_{m+1} > \frac{\abs*{\bar{x}^{m+1}_{\min}}\mathbf{u}}{\bar{L}_{m+1}}
\end{align*}
Similarly for other two dimensions, we get $\epsilon_{m+1} > \frac{\abs*{\bar{y}^{m+1}_{\min}}\mathbf{u}}{\bar{L}_{m+1}}$ and
$\epsilon_{m+1} > \frac{\abs*{\bar{z}^{m+1}_{\min}}\mathbf{u}}{\bar{L}_{m+1}}$.
Thus, we can write $\epsilon_{m+1} > \frac{\abs*{\bar{\mathbf{x}}^{m+1}_{\min}}\mathbf{u}}{\bar{L}_{m+1}}$ which is the
same as writing $\epsilon_{m} > \frac{\abs*{\bar{\mathbf{x}}^{m}_{\min}}\mathbf{u}}{\bar{L}_{m}}$.

