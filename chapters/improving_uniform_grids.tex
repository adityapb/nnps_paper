Uniform grids make use of the fact that the search radius of all the particles in 
the dataset is known before-hand. Thus the space can be partitioned 
into bins of lengths equal to the search radius. However, in cases where we know 
the search radius before-hand but it is not the same for all particles, the space 
cannot be partitioned into bins as done earlier.

An obvious solution to this is to partition the space into bins of sizes equal to the
maximum search radius. However, in this case, we will end up checking a large number of
particles. To solve this problem, the uniform grid method needs to be improved for
cases of variable search radius.

\subsection*{Extended Uniform Grids}

While most SPH implementations use uniform grids with cell size equal to search radius
of particles in case of constant search radius (as cache misses slow down the process even after
having to check lesser number of particles~\citep{Ihmsen} when smaller bins are used), using a
sub-cell resolution can infact be useful in cases of variable search radius.

Define the hash map for the uniform grid as follows,
\[ M: \mathbb{S} \to \mathbb{I} \]
where $\mathbb{I}$ denotes the set of all cell id's in the mesh.

Define a function $f: \mathbb{M} \to \mathbb{R}$ that maps a hash map to it's cell size.
Where $\mathbb{M}$ is a set of all hash maps and $\mathbb{R}$ denotes set of all real numbers. Thus,
\[ f(M) = \max_{k \in \mathbb{S}} h_k \]

Extended uniform grid sub-divides the grid such that the size of each cell is 
$\frac{f(M)}{H}$.

Let the extended uniform grid for the same $\mathbb{S}$ be represented for a hash-map $M'$ as,
\[ M': \mathbb{S} \to \mathbb{I'} \]
\[ f(M') = \frac{f(M)}{H} \]

Define \(\mathcal{H}\)-mask for particle $q$ in a hash map $M'$ as 
\[ \mathcal{H}(M', q) = \{ (s, t, u) \ | \ s, t, u \in \{ -H(M', q), \ldots, H(M', q) \} \} \]
\[ H(M', q) = \ceil*{\frac{h_q}{f(M')}} \]

Define neighborhood $N(M', q)$ of particle $q$ 
as~\footnote{$M'(q) + m = (i, j, k) + (s, t, u) = (i+s, j+t, k+u)$},
\[ N(M', q) = \{ M'(q) + m \ | \ m \in \mathcal{H}(M', q) \} \]

The neighbors of particle $q$ are then given by,
\[ \mathcal{N}(M', q) = \{ \ j \ \forall \ j \in N(M', q) \ | \
||\mathbf{r}_q - \mathbf{r}_j|| < h_q \} \]
\[ \tilde{\mathcal{N}}(M', q) = \{ \ j \ \forall \ j \in N(M', q) \ | \
||\mathbf{r}_q - \mathbf{r}_j|| < \max{(h_q, h_j)} \} \]

Note that,
\[ \mathcal{N}(q) \subseteq \bigcup\limits_{p \in N(M', q)} \mathbb{S}_p \]

However,
\[ \tilde{\mathcal{N}}(q) \not\subseteq \bigcup\limits_{p \in N(M', q)} \mathbb{S}_p \]

Thus, $\mathcal{N}(M', q) = \mathcal{N}(q)$ but 
$\tilde{\mathcal{N}}(M', q) \neq \tilde{\mathcal{N}}(q)$.
Thus the neighborhood $N(M', q)$ is asymmetric.

\subsubsection*{Symmetricity of neighbors}

To find symmetric neighbors, the $\mathcal{H}$-mask defined in previous section is modified
to $\tilde{\mathcal{H}}(M', q)$ as follows.
\[ \tilde{\mathcal{H}}(M', q) = \{ (s, t, u) \ | \ s, t, u \in \{ -H, \ldots, H \} \} \]

Now if the neighborhood is re-defined using the new $\tilde{\mathcal{H}}$-mask,
\[ \tilde{N}(M', q) = \{ M'(q) + m \ | \ m \in \tilde{\mathcal{H}}(M', q) \} \]

Note that,
\[ \tilde{\mathcal{N}}(q) \subseteq \bigcup\limits_{p \in \tilde{N}(M', q)} \mathbb{S}_p \]

However, the size of the mask can become quite large if the neighborhood is re-defined this way.

To solve this problem, the neighborhood is modified as follows,

Let $n = (s, t, u)$.
\[ N^{sym}(M', q) = \{ p \ \forall \ p \in \tilde{N}(M', q) \ | \ p \in N_p(M', q) \} \]
\[ N_p(M', q) = \{M'(q) + n \ | \
s, t, u \in \{ -H^{sym}(M', p, q), \ldots, H^{sym}(M', p, q) \} \} \]
\[ H^{sym}(M', p, q) = \ceil*{\frac{\max(h_q, \hat{h}_p)}{f(M')}} \]
\[ \hat{h}_p = \max_{k \in \mathbb{S}_p} h_k \]

\noindent{\textbf{Proof:}} \\

Consider a cell $p \in \tilde{N}(M', q)$.
Define a new set of particles \(\mathbb{C} = \mathbb{S}_p \cup \{q\}\)
in the same hash map $M'$.

Note that in figure~\ref{fig:sym_mask}, the spherical region of radius $r$ contains all symmetric neighbors
of $q$ in $\mathbb{C}$ where,
\begin{align*}
r &= \max_{k \in \mathbb{C}} h_k \\
  &= \max(h_q, \hat{h}_p) \\
\end{align*}

\begin{figure}[h]
\centering
\begin{subfigure}{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/improving_uniform_grids/mask_in.png}
\caption{}
\label{fig:in}
\end{subfigure}%
\hspace{.1\textwidth}
\begin{subfigure}{.4\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/improving_uniform_grids/mask_out.png}
\caption{}
\label{fig:out}
\end{subfigure}
\caption{$\tilde{N}(M', q)$ for $H=3$. The query particle, $q$ lies in the inner bold cell.
The outer bold box is $N_p(M', q)$ where $p$ is the shaded box.
The dotted box is the bounding box for all spheres of radius $r$ centered anywhere in the inner bold cell.
Figure~\ref{fig:in} is the case of $p \in N_p(M', q)$ and figure~\ref{fig:out} is 
the case of $p \notin N_p(M', q)$}
\label{fig:sym_mask}
\end{figure}

From figure~\ref{fig:sym_mask}, it can be seen that in the case where $p \notin N_p(M',q)$,
there won't be any neighbors of $q$ in $p$. Thus, only the cells which belong to $N_p(M',q)$
need to be checked for neighbors. Also, as we know that
$\tilde{\mathcal{N}}(q) \subseteq \cup_{p \in \tilde{N}(M', q)} \mathbb{S}_p$,
only $\tilde{N}(M',q)$ needs to be checked for cells which belong to $N_p(M',q)$.

From figure~\ref{fig:sym_mask}, it is clear that the neighborhood $N^{sym}(M', q)$ that contains 
all symmetric neighbors is,
\[ N^{sym}(M', q) = \{ p \ \forall \ p \in \tilde{N}(M', q) \ | \ p \in N_p(M', q) \} \]
\[ N_p(M', q) = \{M'(q) + n \ | \
s, t, u \in \{ -H^{sym}(M', p, q), \ldots, H^{sym}(M', p, q) \} \} \]
\begin{align*}
H^{sym}(p,q) &= \ceil*{\frac{r}{f(M')}} \\
&= \ceil*{\frac{\max(h_q, \hat{h}_p)}{f(M')}}
\end{align*}

\subsubsection*{Disadvantages of using the $N^{sym}$ neighborhood}

Even though the $N^{sym}$ neighborhood can cause a significant reduction in the cells to be
scanned for neighbors in the cases of variable search radius, note that the memory required to store the
mask varies for each query point.
Thus, either a memory of $\mathcal{O}({(2H+1)}^3)$ needs to be allocated which leads to a large memory 
requirement, or use of dynamic allocation and resizing which will be slower.

Also, note that if the distribution of search radius across the particles is random, use of 
$N^{sym}$ neighborhood will not help much.

Also, using a sub-divided grid will still give a bad cache performance no matter which mask is used.

We will discuss an alternate approach in the next subsection which also deals with the problem of large
neighborhoods without the disadvantages discussed above.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Stratified Grids}

Inspired by multiple background grids~\citep{Zheng} a new method based on classification of
particles in different grids based on their support radius is presented which solves the 
problem of large bin sizes.

The particles are classified into different levels based on their support radius and then
into bins based on spatial location.

Define the interval size, $\delta$, for classification of particles as
\[ \delta = \left(\frac{h_{\max} - h_{\min}}{n}\right)(1 + \epsilon) \]

where $n$ is the number of levels and $\epsilon$ is a very small number larger than the machine epsilon.

Consider a set of sets of particles $\{ \mathbb{P}_1, \ldots, \mathbb{P}_{n} \}$ such that,
\[ \mathbb{P}_m = \{ i \ \forall \ i \in \mathbb{S} \ | \ 
h_{\min} + (m-1)\delta \leq h_i < h_{\min} + m\delta \} \]

Note that, $\mathbb{S} = \bigcup\limits_{i = 1}^{n} \mathbb{P}_i$ and 
$\mathbb{P}_i \cap \mathbb{P}_j = \phi$ for $0 \leq i \neq j < n$.

Define a set of uniform grids $\{ M_1, \ldots, M_{n} \}$ such that 
$M_{j} : \mathbb{P}_j \to \mathbb{I}_j$ and $f(M_j) = \hat{h}_j$.

The $\mathcal{H}$-mask at level $j$ is given by,
\[ \mathcal{H}(M_j, q) = \{ (s, t, u) \ | \ 
s, t, u \in \{ -H(M_j, q), \ldots, H(M_j, q) \} \} \]
\[ H(M_j, q) = \ceil*{\frac{\max(h_q, f(M_j))}{f(M_j)}} \]

Use the following neighborhood for finding neighbors, $j$ being the level of particle 
classification.
\[ N(M_j, q) = \{ M_j(q) + m \ | \ m \in \mathcal{H}(M_j, q) \} \]
\[ \mathcal{N}_j(q) = \{ \ k \ \forall \ k \in \mathbb{P}_j(p) \ | \ p \in N(M_j, q)\}\]
\[ \mathcal{N}(q) = \bigcup\limits_{j = 1}^{n} \mathcal{N}_j(q) \]

$\mathcal{N}_j(q)$ being the neighbors of particle $q$ at level $j$.

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/improving_uniform_grids/stratified_grids.eps}
\caption{Stratified grids for number of levels = $4$}
\label{fig:stratified_grids}
\end{figure}

\subsubsection*{Choosing optimal number of levels}

In theory, greater number of levels should make the nearest neighbor searching faster.
However, due to increased number of cache misses, in practice using a large number of levels
is not feasible.

In practice, increasing the number of levels will initially speed up the computation, however after a
point, further increasing the number of levels will infact slow the algorithm down. Thus there
exists an optimal number of levels at which the speed up is maximum.

To find the optimal number of levels, we will plot time against the number of levels for different
values of $N$ and $\eta$.

Figure~\ref{fig:optimal_n_small_num} shows a plot of time against number of levels for different values 
of $\eta$ with constant $N$.

Note that generally $n = 2$ is the optimal number of levels as seen from figure~\ref{fig:optimal_n_small_num}.
Only in the case of large number of neighbors, $n = 3$ is marginally better. However, in practice,
number of neighbors is generally small. Thus, $n = 2$ is the best choice of number of levels.

$k_{\max} = Nh_{\max}^3$ is the maximum number of particles in a cell and $k_{\min} = Nh_{\min}^3$ is the
minimum number of particles in a cell in a uniform distribution of particles for the same $N$, 
$h_{\min}$ and $h_{\max}$. $h_{\min}$ and $h_{\max}$ is scaled with $N$ such that $k_{\min}$ and $k_{\max}$
remains constant.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/improving_uniform_grids/plot_optimal_num_levels_N_ratio_2.eps}
\caption{}
\label{fig:optimal_n_ratio_2}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/improving_uniform_grids/plot_optimal_num_levels_N_ratio_10.eps}
\caption{}
\label{fig:optimal_n_ratio_10}
\end{subfigure}

\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/improving_uniform_grids/plot_optimal_num_levels_N_ratio_20.eps}
\caption{}
\label{fig:optimal_n_ratio_20}
\end{subfigure}
\caption{Figure~\ref{fig:optimal_n_ratio_2} shows $\frac{T}{T_{n=1}}$ vs $n$ for $\eta = 2.5$ and 
figure~\ref{fig:optimal_n_ratio_10} shows $\frac{T}{T_{n=1}}$ vs $n$ for $\eta = 10$ and 
figure~\ref{fig:optimal_n_ratio_20} shows $\frac{T}{T_{n=1}}$ vs $n$ for $\eta = 20$}
\label{fig:optimal_n_small_num}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/improving_uniform_grids/plot_optimal_num_levels_N_ratio_3.eps}
\caption{$\frac{T}{T_{n=1}}$ vs $n$ for $\eta = 3$ for a large number of neighbors}
\end{figure}

