This chapter compares the algorithms discussed in the previous chapters using various
metrics such as build times and nearest neighbor search times. All the benchmarks are
run using \texttt{PySPH}~\citep{PySPH}.

\subsection*{Comparison of build times}

This section will compare the build times for the algorithms discussed in previous chapters.
For serial runs, the build times hardly matter. They do however matter a lot while
running these algorithms in parallel. The latter is not in the scope of this report
and has been left for future work but will be briefly discussed in the next chapter.

\subsubsection*{Comparison for uniform grid based algorithms}

Figure~\ref{fig:ll_sfc_build} shows a comparison of build times for algorithms based on uniform grids.
The benchmarks have been run on a 3D uniform particle distribution.
\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/build_time_sh_ll_sfc.eps}
\caption{Comparison of build times for algorithms based on uniform grids}
\label{fig:ll_sfc_build}
\end{figure}

Space filling curve requires sorting and thus has a larger build time than other
uniform grid based algorithms.

\subsubsection*{Comparison of tree and uniform grids}

Figure~\ref{fig:tree_ll_build} shows a comparison of build times for octree and uniform grids based linked
list algorithm.
\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/build_time_tree_ll.eps}
\caption{Comparison of build times for octree and linked list algorithm}
\label{fig:tree_ll_build}
\end{figure}

Octree has a time complexity of $\mathcal{O}(N\log{N})$ for building. Thus is slower than other
uniform grid based algorithms.

\subsection*{Comparison of nearest neighbor search times}

\subsubsection*{Constant search radius}

In this subsection, the algorithms will be with datasets of constant search radius
such that $Nh^3 = k$. That is, the number of neighbors as $N$ is scaled up stays the same. \\

\noindent{\textbf{Comparison of tree and uniform grids}}

Octree, being a recursive algorithm and having a time complexity of $\mathcal{O}(N\log{N})$,
is slower than uniform grid based algorithms in the case of constant $h$ as seen in
figure~\ref{fig:tree_ll_const_h}. \\

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/time_tree_ll_const_h.eps}
\caption{Comparison of octree and linked list for constant $h$ and uniform distribution
of particles.}
\label{fig:tree_ll_const_h}
\end{figure}

\noindent{\textbf{Comparison of uniform grid based algorithms}}

Figure~\ref{fig:ll_sh_sfc_ci_const_h} shows the comparison of uniform grid based algorithms for 
constant $h$ and uniform distribution of particles. Space filling curve and linked list is
faster than spatial hash because of better cache performance.

However, linked list also stores empty cells in the uniform grid. Thus, for a dataset like
the one in figure~\ref{fig:pat_data}, linked list will be much slower as shown in 
figure~\ref{fig:pat_ll_sh_sfc_const_h}.

Also space filling curve is faster than cell indexing because of better cache performance
as can be seen from figure~\ref{fig:ll_sh_sfc_ci_const_h}.

\begin{figure}
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_ll_sh_sfc_ci_const_h.eps}
\caption{}
\label{fig:ll_sh_sfc_ci_const_h}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_ll_sh_sfc_const_h_pat.eps}
\caption{}
\label{fig:pat_ll_sh_sfc_const_h}
\end{subfigure}
\caption{Comparison of uniform grid based algorithms for constant $h$ 
on~\subref{fig:ll_sh_sfc_ci_const_h} uniform distribution of particles~\subref{fig:pat_ll_sh_sfc_const_h}
dataset shown in figure~\ref{fig:pat_data}}
\end{figure}

\subsubsection*{Variable search radius}

Similar to the previous subsection, in this subsection $Nh_{\max}^3 = k_{\max}$ and $Nh_{\min}^3 = k_{\min}$,
such that $k_{\max}$ and $k_{\min}$ remain constant with $N$.

The dataset in figure~\ref{fig:grad_rad} is such that $h \propto dx$ where $dx$ is the
interparticle spacing. This dataset will be called gradient radius dataset in the following 
subsections.

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/make_gradient_radius.eps}
\caption{Dataset with a gradient of search radius from $h_{\min}$ to $h_{\max}$}
\label{fig:grad_rad}
\end{figure}

Figures~\ref{fig:sh_sr_tree_ratio_2} and~\ref{fig:sh_sr_tree_ratio_10} show the comparison of 
spatial hash, stratified grid based on spatial hash and octree for a small and a large $\eta$ 
on the dataset showed in figure~\ref{fig:grad_rad}.

Space filling curve is being compared only with spatial hash as \texttt{PySPH} has stratified
grids based on spatial hash. The speed up will be approximately the same for other algorithms.

For a large number of neighbors, extended uniform grids performs better than uniform grids
but is slower than stratified grids as seen from figure~\ref{fig:sh_esh_sr_grad_ratio_4}.
For smaller number of particles, extended uniform grids is drastically slower than
both uniform grids and stratified grids as seen in figure~\ref{fig:sh_esh_sr_small_neigh}. \\

\begin{figure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_sh_sr_tree_grad_ratio_2.eps}
\caption{}
\label{fig:sh_sr_tree_ratio_2}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_sh_sr_tree_grad_ratio_10.eps}
\caption{}
\label{fig:sh_sr_tree_ratio_10}
\end{subfigure}
\caption{Comparison of spatial hash, octree and stratified uniform grids for $\eta = 2.5$ in 
figure~\ref{fig:sh_sr_tree_ratio_2} and $\eta = 10$ in figure~\ref{fig:sh_sr_tree_ratio_10}}
\end{figure}

\begin{figure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_sh_esh_sr_grad_ratio_4.eps}
\caption{}
\label{fig:sh_esh_sr_grad_ratio_4}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=\linewidth]{images/chapters/results/time_sh_esh_sr_grad_small_neigh.eps}
\caption{}
\label{fig:sh_esh_sr_small_neigh}
\end{subfigure}
\caption{Comparison of spatial hash, extended uniform grid based on spatial hash, stratified
grids based on spatial hash for~\subref{fig:sh_esh_sr_grad_ratio_4} $H = 4$, $\eta = 4$ for 
a large number of neighbors~\subref{fig:sh_esh_sr_small_neigh} $H = 5$, $\eta = 2.5$ for a 
small number of neighbors}
\end{figure}

\noindent{\textbf{Comparison of octree and compressed octree}}

Observe that compressed octree is marginally faster than octree. Also compressed octree
has lesser memory requirement as shown in table~\ref{mem_tree_comp_tree}. \\

\begin{table}
\centering
\begin{tabular}{| c | c | c |}
\hline
Number of particles         &   Octree (MB)     &   Compressed octree (MB)   \\
\hline
1000001                     &   20              &   10                   \\
8000001                     &   153             &   91                   \\
\hline
\end{tabular}
\caption{Memory usage for octree and compressed octree for dataset shown in figure~\ref{fig:pat_data}.}
\label{mem_tree_comp_tree}
\end{table}

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/time_tree_comp_tree_grad_ratio_10.eps}
\caption{Comparison of octree and compressed octree for $\eta = 10$}
\label{fig:tree_comp_tree_ratio_10}
\end{figure}

\noindent{\textbf{Comparison with \texttt{NanoFLANN}}}

Figure~\ref{fig:flann_tree_ll_ratio_10} shows the comparison of \texttt{NanoFLANN} with octree and 
linked list. Note that \texttt{NanoFLANN} is marginally faster than octree. However, 
\texttt{NanoFLANN} gives approximate results as shown in table~\ref{flann_accuracy}.

\begin{figure}
\centering
\includegraphics[width=.6\linewidth]{images/chapters/results/time_flann_tree_ll_grad_ratio_10.eps}
\caption{Comparison of linked list, \texttt{NanoFLANN} and octree for $\eta = 10$}
\label{fig:flann_tree_ll_ratio_10}
\end{figure}

\begin{table}
\centering
\begin{tabular}{| c | c | c |}
\hline
Number of particles         &   Average accuracy (\%)   &   Minimum accuracy (\%)   \\
\hline
343000                      &   98.86                   &   57.89                   \\
512000                      &   99.01                   &   63.15                   \\
729000                      &   99.12                   &   63.15                   \\
\hline
\end{tabular}
\caption{Accuracy of \texttt{NanoFLANN} for dataset shown in figure~\ref{fig:grad_rad} for $\eta = 2$}
\label{flann_accuracy}
\end{table}

