\documentclass[../report.tex]{subfiles}

\begin{document}

In the following sections the time complexities for extended uniform grids
and stratified grids are derived.

\section{Extended Uniform Grids}

Note that the time for nearest neighbor searching, $T$ is such that,
\begin{align*}
T &= \mathcal{O}\left((2H+1)^3 \frac{h_{\max}^3}{H^3} N^2\right) \\
&= \mathcal{O}\left( \left(2 + \frac{1}{H}\right)^3 h_{\max}^3 N^2 \right)
\end{align*}

Thus, for a limiting case, $H \to \infty$,
\[ T = \mathcal{O}(8 h_{\max}^3 N^2) \]
\[ T = \mathcal{O}(8 \eta^3 h_{\min}^3 N^2) \]

\section{Stratified Grids}

Let $f(m)$ be the number of particles at level $m$.
Time for nearest neighbor searching using stratified grids is,
\begin{align*}
T &= \mathcal{O}\left( \sum_{q = 1}^{N} \sum_{m = 1}^{n} (2H_m(q)+1)^3 f(m) \hat{h}_m^3 \right) \\
&= \mathcal{O}\left( \sum_{m = 1}^{n} \sum_{q = 1}^{N} (2H_m(q)+1)^3 f(m) \hat{h}_m^3 \right)
\end{align*}

Let $\tilde{f}(m) = \frac{N}{n}f(m)$.

\begin{align*}
T &= \mathcal{O}\left( \frac{N}{n} \sum_{m = 1}^{n} \tilde{f}(m) \hat{h}_m^3 \sum_{q = 1}^{N} (2H_m(q)+1)^3 \right)
\end{align*}

Consider two sets of particles, $\mathbb{N}_1$ and $\mathbb{N}_2$ such that 
$\mathbb{N}_1 = \bigcup\limits_{i = 1}^{m} \mathbb{P}_i$ and $\mathbb{N}_2 = \bigcup\limits_{i = m+1}^{n} \mathbb{P}_i$.

Note that $H_m(q) = 1 \ \forall \ q \in \mathbb{N}_1$ and 
$H_m(q) = \frac{h_q}{\hat{h}_m} \ \forall \ q \in \mathbb{N}_2$.
\begin{align*}
\sum_{q = 1}^{N} (2H_m(q)+1)^3 &= \sum_{q \in \mathbb{N}_1} 27 + \sum_{q \in \mathbb{N}_2} \
\left( \frac{2h_q}{\hat{h}_m} + 1 \right)^3
\end{align*}

Note that,
\[ \sum_{q \in \mathbb{N}_1} 27 = \frac{27N}{n}\sum_{i = 1}^{m} \tilde{f}(i) \]
\[ \sum_{q \in \mathbb{N}_2} \left( \frac{2h_q}{\hat{h}_m} + 1 \right)^3 = \
\frac{N}{n}\sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_m} + 1 \right)^3 \tilde{f}(i) \]

such that $\bar{h}_i = \frac{\hat{h}_i + \tilde{h}_i}{2}$.

Note that,
\[ \frac{\delta}{h_{\min}} = \frac{\eta - 1}{n} \]

Also define $\tilde{g}(x) = \tilde{f}(nx)$
\begin{align*}
\frac{N}{n}\sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_m} + 1 \right)^3 \tilde{f}(i) &= \
\frac{N}{n\hat{h}^3_m}\sum_{i = m+1}^{n} \left( 3h_{\min} + \delta(2i + m - 1) \right)^3 \tilde{f}(i) \\
&= \frac{27h^3_{\min}N}{\hat{h}^3_m} \sum_{i = m+1}^{n} \ 
\left( 1 + \frac{\eta - 1}{3}\left( 2\frac{i}{n} + \frac{m}{n} - \frac{1}{n} \right) \right)^3 \tilde{g}\left(\frac{i}{n}\right) \frac{1}{n}
\end{align*}

In a limiting case when $n \to \infty$,
\begin{align*}
\frac{N}{n}\sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_m} + 1 \right)^3 \tilde{f}(i) &=
\frac{27h^3_{\min}N}{\hat{h}^3_m} \int_{m/n}^{1} \tilde{g}(\xi) \left( 1 + \frac{\eta - 1}{3}\left( 2\xi + \frac{m}{n} \right) \right)^3 d\xi
\end{align*}

Also,
\begin{align*}
\frac{27N}{n}\sum_{i = 1}^{m} \tilde{f}(i) &= 27N \int_{0}^{m/n} \tilde{g}(\xi) d\xi
\end{align*}

Let,
\[ I_1 = \int_{0}^{m/n} \tilde{g}(\xi) d\xi \]
\[ I_2 = \int_{m/n}^{1} \tilde{g}(\xi) \left( 1 + \frac{\eta - 1}{3}\left( 2\xi + \frac{m}{n} \right) \right)^3 d\xi \]

Therefore,
\[ \sum_{q = 1}^{N} (2H_m(q)+1)^3 = 27NI_1 + \frac{27h^3_{\min}N}{\hat{h}^3_m}I_2 \]

Thus,
\begin{align*}
\frac{N}{n} \sum_{m = 1}^{n} \tilde{f}(m) \hat{h}_m^3 \sum_{q = 1}^{N} (2H_m(q)+1)^3 &= \
27 N^2 h^3_{\min} \sum_{m = 1}^{n} \left( \frac{\tilde{f}(m)\hat{h}^3_m}{h^3_{\min}} I_1 + \tilde{f}(m)I_2 \right) \frac{1}{n}
\end{align*}
\begin{align*}
\sum_{m = 1}^{n} \frac{\tilde{f}(m)\hat{h}^3_m}{h^3_{\min}} I_1 &= \
\sum_{m = 1}^{n} \tilde{f}(m)\left( 1 + (\eta - 1)\frac{m}{n} \right)^3I_1 \\
&= n \int_{0}^{1} \tilde{g}(\zeta)(1 + (\eta - 1)\zeta)^3 I_1 d\zeta \\
&= n \int_{0}^{1} \tilde{g}(\zeta)(1 + (\eta - 1)\zeta)^3 \int_{0}^{\zeta} \tilde{g}(\xi) d\xi d\zeta \\
&= n\tilde{I}_1
\end{align*}
\begin{align*}
\sum_{m = 1}^{n} \tilde{f}(m)I_2 &= n \int_{0}^{1} \tilde{g}(\zeta) I_2 d\zeta \\
&= n \int_{0}^{1} \tilde{g}(\zeta) \int_{\zeta}^{1} \tilde{g}(\xi) \
\left( 1 + \frac{\eta - 1}{3}\left( 2\xi + \zeta \right) \right)^3 d\xi d\zeta \\
&= n\tilde{I}_2
\end{align*}

Thus,
\[ T = \mathcal{O}\left( 27 N^2 h^3_{\min} \left( \tilde{I}_1 + \tilde{I}_2 \right) \right) \]

Consider a case where $\tilde{f}(m) = 1$, that is a constant variation of $h$ for all particles. Note that $\tilde{g}(x) = 1$.
\begin{align*}
\tilde{I}_1 &= \int_{0}^{1} \zeta (1 + (\eta - 1)\zeta)^3 d\zeta
\end{align*}
Using integration by parts,
\begin{align*}
\tilde{I}_1 &= \zeta \int (1 + (\eta - 1)\zeta)^3 d\zeta \ \Big|_{0}^{1} - \
\int \int (1 + (\eta - 1)\zeta)^3 d\zeta d\zeta \ \Big|_{0}^{1} \\
&= \frac{\eta^4}{4(\eta - 1)} - \frac{\eta^5 - 1}{20(\eta - 1)^2} \\
&= \frac{5\eta^4 - \eta^3 - \eta^2 - \eta - 1}{20(\eta - 1)}
\end{align*}
\[ \tilde{I}_2 = \int_{0}^{1} \int_{\zeta}^{1} \left( 1 + \frac{\eta - 1}{3}\left( 2\xi + \zeta \right) \right)^3 d\xi d\zeta \]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\begin{align*}
\sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_i} + 1 \right)^3 \tilde{f}(i) &= \
\sum_{i = m+1}^{n} \left( \frac{\tilde{h}_i}{\hat{h}_i} + 2 \right)^3 \tilde{f}(i) \\
&= \sum_{i = m+1}^{n} \left( \frac{h_{\min} + \delta(i - 1)}{h_{\min} + \delta i} + 2 \right)^3 \tilde{f}(i) \\
&= \sum_{i = m+1}^{n} \left( 3 - \frac{\delta}{\hat{h}_i} \right)^3 \tilde{f}(i) \\
&= \sum_{i = m+1}^{n} 27\left( 1 - \frac{\delta}{3\hat{h}_i} \right)^3 \tilde{f}(i) \\
&= \sum_{i = m+1}^{n} 27\left( 1 - \frac{\delta}{\hat{h}_i} + \mathcal{O}(\delta^2) \right) \tilde{f}(i)
\end{align*}

In the limiting case where $n \to \infty$, higher order terms of $\delta$ can be ignored. Thus,
\[ \sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_i} + 1 \right)^3 \tilde{f}(i) = \
\sum_{i = m+1}^{n} 27\left( 1 - \frac{\delta}{\hat{h}_i} \right) \tilde{f}(i)\]

Similarly,
\begin{align*}
\frac{\delta}{\hat{h}_i} &= \frac{\delta}{h_{\min} + i\delta} \\
&= \frac{\delta}{h_{\min}} \left( 1 + \frac{i\delta}{h_{\min}} \right)^{-1}
\end{align*}

Thus,
\begin{align*}
\sum_{i = m+1}^{n} 27\left( 1 - \frac{\delta}{\hat{h}_i} \right) \tilde{f}(i) &= \
27\sum_{i = m+1}^{n} \tilde{f}(i) - \frac{\delta}{h_{\min}}\sum_{i = m+1}^{n} \frac{\tilde{f}(i)}{1 + \frac{i\delta}{h_{\min}}}
\end{align*}

Let $\tilde{g}(i) = \tilde{f}(ni)$. Also,
\[ \frac{\delta}{h_{\min}} = \frac{\eta - 1}{n} \]

Thus,
\begin{align*}
\sum_{i = m+1}^{n} \frac{\tilde{f}(i)}{1 + \frac{i\delta}{h_{\min}}} &= \sum_{i = m+1}^{n} \
\frac{\tilde{g}(\frac{i}{n})}{1 + (\eta - 1)\frac{i}{n}} \\
&= n\sum_{i = m+1}^{n} \frac{\tilde{g}(\frac{i}{n})}{1 + (\eta - 1)\frac{i}{n}} \frac{1}{n} \\
&= n\int_{m/n}^{1} \frac{\tilde{g}(x)}{1 + (\eta - 1)x} dx
\end{align*}

Thus,
\[ \sum_{i = m+1}^{n} 27\left( 1 - \frac{\delta}{\hat{h}_i} \right) \tilde{f}(i) = \
27\sum_{i = m+1}^{n} \tilde{f}(i) - (\eta - 1)\int_{m/n}^{1} \frac{\tilde{g}(x)}{1 + (\eta - 1)x} dx \]
\[ \sum_{i = m+1}^{n} \left( \frac{2\bar{h}_i}{\hat{h}_i} + 1 \right)^3 \tilde{f}(i) + 27\sum_{i = 1}^{m} \tilde{f}(i) = \
27\sum_{i = 1}^{n} \tilde{f}(i) - (\eta - 1)\int_{m/n}^{1} \frac{\tilde{g}(x)}{1 + (\eta - 1)x} dx \]

Note that $\sum_{i = 1}^{n} \tilde{f}(i) = n$. Thus,
\[ \sum_{q = 1}^{N} (2H_m(q)+1)^3 = 27N - \frac{(\eta - 1)N}{n} \int_{m/n}^{1} \frac{\tilde{g}(x)}{1 + (\eta - 1)x} dx \]

Define $I$ as follows,
\[ I = \int_{m/n}^{1} \frac{\tilde{g}(x)}{1 + (\eta - 1)x} dx \]

Note that,
\begin{align*}
\hat{h}_m^3 &= h^3_{\min}\left( 1 + \frac{\delta m}{h_{\min}} \right)^3 \\
&= h^3_{\min}\left( 1 + (\eta - 1)\frac{m}{n} \right)^3
\end{align*}

Thus,
\[ \sum_{m = 1}^{n} \tilde{f}(m) \hat{h}_m^3 \sum_{q = 1}^{N} (2H_m(q)+1)^3 = \
h_{\min}^3 \left( \sum_{m = 1}^{n} \tilde{f}(m) \left( 1 + (\eta - 1)\frac{m}{n} \right)^3 \
\left( 27N - \frac{(\eta - 1)N}{n}I \right) \right) \]

As $n \to \infty$, $\frac{(\eta - 1)N}{n} \to 0$. Thus,
\begin{align*}
\frac{N}{n} \sum_{m = 1}^{n} \tilde{f}(m) \hat{h}_m^3 \sum_{q = 1}^{N} (2H_m(q)+1)^3 &= \
27 N^2 h_{\min}^3 \sum_{m = 1}^{n} \tilde{f}(m)\left( 1 + (\eta - 1)\frac{m}{n} \right)^3\frac{1}{n} \\
&= 27 N^2 h_{\min}^3 \int_{0}^{1} \tilde{g}(\xi)(1  + (\eta - 1)\xi)^3 d\xi
\end{align*}

Let $T_0 = 27 N^2 h_{\max}^3$. Thus,
\[ T = \mathcal{O}\left( \frac{T_0}{\eta^3} \int_{0}^{1} \tilde{g}(\xi)(1  + (\eta - 1)\xi)^3 d\xi \right) \]

Recall that the time complexity for extended uniform grids in terms of $T_0$ is,
\[ T = \mathcal{O}\left( T_0 \right) \]

Consider a case when all particles have a constant $h$. That is when $\eta = 1$.
\[ T = \mathcal{O}\left( T_0 \int_{0}^{1} \tilde{g}(\xi) d\xi \right) \]

Note that,
\[ \int_{0}^{1} \tilde{g}(\xi) d\xi = 1 \]

Thus, for constant $h$, $T = \mathcal{O}(T_0)$, same as extended uniform grids.

Consider a case where $\tilde{f}(m) = 1$. Thus $\tilde{g}(\xi) = 1$. Thus,
\begin{align*}
\int_{0}^{1} \tilde{g}(\xi)(1  + (\eta - 1)\xi)^3 d\xi &= \int_{0}^{1} (1  + (\eta - 1)\xi)^3 d\xi \\
&= \frac{(1  + (\eta - 1)\xi)^4}{4(\eta - 1)} \ \Big|_0^1 \\
&= \frac{\eta^4 - 1}{4(\eta - 1)} \\
&= \frac{1}{4}(\eta^2 + 1)(\eta + 1) \\
&= \frac{1}{4}(\eta^3 + \eta^2 + \eta + 1)
\end{align*}

Thus,
\[ T = \mathcal{O}\left( \frac{T_0}{4} \left(1 + \frac{1}{\eta} + \frac{1}{\eta^2} + \frac{1}{\eta^3}\right) \right) \]

Let the constant factor in the expression for time complexity be $k$.
\[ k = \frac{1}{4}\left( 1 + \frac{1}{\eta} + \frac{1}{\eta^2} + \frac{1}{\eta^3} \right) \]

Note that $k < 1$ for $\eta > 1$. Thus, for the cases of variable $h$, stratified grids is theoretically
faster than extended uniform grids.

In the above analysis for stratified grids we have considered sub-division factor for uniform grids equal
to one. If we consider a sub-division factor, $H$, the expression for time complexity will stay the same with
the value of $T_0$ changed to
\[ T_0 = \left( 2 + \frac{1}{H} \right) N^2 h_{\max}^3 \]


%Note that,
%\[ \int_{0}^{1} \tilde{g}(\xi) d\xi = 1 \]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Note that,
\[ \int_{0}^{1} \tilde{g}(\xi)d\xi = 1 \]

Thus,
\begin{align*}
T = \mathcal{O}\left( 27 N^2 h_{\min}^3 \left( 1 + (\eta - 1) \int_{0}^{1} \xi \tilde{g}(\xi) d\xi \right) \right)
\end{align*}

Let $T_0 = 27 N^2 h_{\min}^3$. Thus,
\begin{align*}
T = \mathcal{O}\left( T_0 \left( 1 + (\eta - 1) \int_{0}^{1} \xi \tilde{g}(\xi) d\xi \right) \right)
\end{align*}

In the above analysis for stratified grids we have considered sub-division factor for uniform grids equal
to one. If we consider a sub-division factor, $H$, the expression for time complexity will stay the same with
the value of $T_0$ changed to
\[ T_0 = \left( 2 + \frac{1}{H} \right) N^2 h_{\min}^3 \]

Note that the time complexity for extended uniform grids in terms of $T_0$ is,
\[ T = \mathcal{O}\left( T_0\eta^3 \right) \]

\section{Comparing extended uniform grids and stratified grids}

For stratified grids to be faster than extended uniform grids,
\[ T_0 \left( 1 + (\eta - 1) \int_{0}^{1} \xi \tilde{g}(\xi) d\xi \right) < T_0\eta^3 \]
\[ \implies \int_{0}^{1} \xi \tilde{g}(\xi) d\xi < \eta^2 + \eta + 1 \]

Define $I_{\xi}$ such that,
\[ I_{\xi} = \int_{0}^{1} \xi \tilde{g}(\xi) d\xi \]

Thus, for stratified grids to be faster, we need $\eta$ such that,
\[ \eta^2 + \eta + 1 - I_{\xi} > 0 \]

As $\eta^2 + \eta + 1 - I_{\xi} = y$ is a convex parabola, $y > 0$ when $\eta > \hat{\eta}$
where $\hat{\eta}$ is the larger root of $y = 0$.
\[ \hat{\eta} = \frac{\sqrt{4I_{\xi} - 3}}{2} \]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Observe that,
%\begin{align*}
%\hat{h}_m^3 &= h^3_{\min}\left( 1 + \frac{m\delta}{h_{\min}} \right)^3 \\
%&= h^3_{\min}\left( 1 + \frac{3m\delta}{h_{\min}} \right)
%\end{align*}


%Consider a special case where the distribution of $h$ is uniform. Thus, $\tilde{f}(m) = 1$ or
%$\tilde{g}(x) = 1$.
%
%Thus,
%\begin{align*}
%\sum_{i = m+1}^{n} \frac{\tilde{f}(i)}{1 + \frac{i\delta}{h_{\min}}} &= n\int_{m/n}^{1} \frac{dx}{1 + (\eta - 1)x} \\
%&= \frac{n}{\eta - 1}\log{\left( \frac{\eta}{1 + (\eta - 1)\frac{m}{n}} \right)}
%\end{align*}
%
%Thus,
%\[ \sum_{i = 1}^{m} 27\left( 1 - \frac{\delta}{\hat{h}_i} \right) \tilde{f}(i) = \
%27m - \log{\left(1 + (\eta - 1)\frac{m}{n}\right)} \]
%
%Also,
%\[ \sum_{i = m+1}^{n} \tilde{f}(i) = n-m \]
%
%Thus,
%\begin{align*}
%\sum_{q = 1}^{N} (2H_m(q)+1)^3 &= \frac{N}{n} \left( 27(n-m) + 27m - \log{\left(1 + (\eta - 1)\frac{m}{n}\right)} \right)
%\end{align*}
\end{comment}

\end{document}
