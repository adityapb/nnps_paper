.PHONY: benchmark

all: pdf

benchmark:
	rm -r -f output/
	mkdir output/
	python benchmark/run_benchmarks.py --nnps tree sfc ll bs sh --min-length 40 --max-length 80 \
    	--output-filename output/const_h.csv --benchmarks 6 --leaf-max-particles 100 --num-levels 2
	python benchmark/run_benchmarks.py --nnps tree sfc ll bs sh --min-length 40 --max-length 80 \
    	--output-filename output/const_h_small.csv --h 0.05 --benchmarks 6 --leaf-max-particles 100 \
		--num-levels 2
	python benchmark/run_benchmarks.py --nnps tree sr sfc --min-length 40 --max-length 80 \
    	--h-min 0.1 --h-max 0.2 --output-filename output/grad_ratio_2.csv --benchmarks 2 \
		--leaf-max-particles 100 --num-levels 2
	python benchmark/run_benchmarks.py --nnps tree sr sfc --min-length 40 --max-length 80 \
    	--h-min 0.05 --h-max 0.2 --output-filename output/grad_ratio_4.csv --benchmarks 2 \
		--leaf-max-particles 100 --num-levels 2
	python benchmark/run_benchmarks.py --nnps sh sfc ll bs tree --min-length 40 --max-length 80 \
    	--h 0.1 --output-filename output/pathological_const_h.csv --benchmarks 3 \
		--num-levels 2 --leaf-max-particles 100
	python benchmark/run_benchmarks.py --nnps sfc sr tree --min-length 40 --max-length 80 \
    	--h-min 0.1 --h-max 0.2 --output-filename output/gaussian_ratio_2.csv --benchmarks 1 \
		--num-levels 2 --leaf-max-particles 100

pdf: clean
	mkdir build/
	xelatex -output-directory build/ report.tex
	bibtex build/report
	xelatex -output-directory build/ report.tex
	xelatex -output-directory build/ report.tex

clean:
	rm -r -f build/

