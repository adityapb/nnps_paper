#!/usr/bin/env python

import sys
import numpy as np
from benchmark.benchmark import Benchmark
import os


b = Benchmark()

dir_name = str(sys.argv[1])

for filename in os.listdir(dir_name):
    if filename.endswith(".csv"):
        b.plot_from_csv(dir_name + "/" + filename)

