import json

data =  {'ll': "Linked List",
         'bs': "Box Sort",
         'sh': "Spatial Hash",
         'esh': "Extended Spatial Hash $\mathcal{H}$-mask",
         'sym': "Extended Spatial Hash $\mathcal{H}^{sym}$-mask",
         'sr': "Stratified Grids",
         'tree': "Octree",
         'comp_tree': "Compressed Octree",
         'ci': "Cell Indexing",
         'sfc': "Space Filling Curve",
         'ckdtree': "scipy.spatial.cKDTree",
         'flann': "NanoFLANN"}

colors =  {'ll': 'b',
         'bs': 'y',
         'sh': 'g',
         'esh': 'r',
         'sym': 'm',
         'sr': 'y',
         'tree': 'c',
         'comp_tree' : 'y',
         'ci': 'y',
         'sfc': 'k',
         'ckdtree': '#eeefff',
         'flann' : 'g'}

data = data, colors

with open('config.txt', 'w') as outfile:
    json.dump(data, outfile)

