#!/usr/bin/env python

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import cPickle as pickle
import sys
import numpy as np

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

data = pickle.load(open(str(sys.argv[1]), "rb"))

print data

plot_data = data["plot_data"]
N = data["N"]
num_levels = data["num_levels"]
h_max = data["h_max"]
h_min = data["h_min"]

fig = plt.figure()
ax = fig.add_subplot(111)

for i, n in enumerate(N):
    plot_data[i] /= plot_data[i][0]
    ax.plot(num_levels, plot_data[i],
            "--o", color=np.random.rand(3,1), label="Number of particles = %i" % n**3)

eta = h_max/h_min
k_min = 1 + int((N[0]*h_min)**3)
k_max = 1 + int((N[0]*h_max)**3)

ax.set_ylabel(r"$\frac{T}{T_{n=1}}$")
ax.set_xlabel("Number of levels")
ax.legend(loc="upper left")
ax.set_title("Time vs number of levels for \
        $k_{\min} = %g$, $k_{\max} = %g$" % (k_min, k_max))

plt.savefig(str(sys.argv[1]).split(".")[0] + ".eps", format='eps',
        dpi=1200)

plt.show()

