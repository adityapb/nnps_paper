#!usr/bin/env python
from benchmark import *
import pandas as pd
import time
import argparse
from operator import attrgetter

"""
This script is used for running the benchmarks in benchmark directory.

Usage:

$ python run_benchmarks.py

"""

def _get_subclass_instances():
    subs = Benchmark.__subclasses__()
    return [cls() for cls in subs]

def get_benchmarks():
    instances = _get_subclass_instances()
    benchmarks = []
    for inst in instances:
        benchmarks += [getattr(inst, name) for name in dir(inst)
                if name.startswith('time')]
        sorted(benchmarks, key=attrgetter("__doc__"))
    return benchmarks

def print_benchmarks(benchmarks):
    for i, func in enumerate(benchmarks):
        print i, (func.__doc__ if func.__doc__ is not None else func.__name__)

def run_benchmarks(funcs, options):
    for f in funcs:
        print "Running", "."*20, f.__doc__ if f.__doc__ is not None else f.__name__
        f(options)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--plot-dataset", dest="plot_dataset",
            action="store_true", default=False)

    parser.add_argument("--output-filename", dest="filename",
            type=str, default="result")

    parser.add_argument("--benchmarks", dest="benchmarks",
            nargs="+", type=int, default=[])

    # NNPS options
    parser.add_argument("--N", dest="N", type=int, default=40)

    parser.add_argument("--sub-factor", dest="H", type=int, default=3)

    parser.add_argument("--num-levels", dest="num_levels", type=int, default=1)

    parser.add_argument("--h", dest="h", type=float, default=0.2)

    parser.add_argument("--h-max", dest="h_max", type=float, default=0.2)

    parser.add_argument("--h-min", dest="h_min", type=float, default=0.1)

    parser.add_argument("--std", dest="std", type=float, default=1)

    parser.add_argument("--leaf-max-particles", dest="max_particles",
            type=int, default=10)

    parser.add_argument("--min-particles", dest="min_particles",
            type=int, default=10)

    parser.add_argument("--min-length", dest="min_length",
            type=int, default=15)

    parser.add_argument("--max-length", dest="max_length",
            type=int, default=30)

    parser.add_argument("--param-N", dest="param_N",
            nargs="+", type=int, default=[])

    parser.add_argument("--param-h-max", dest="param_h_max",
            nargs="+", type=float, default=[])

    parser.add_argument("--param-num-levels", dest="param_num_levels",
            nargs="+", type=int, default=[])

    parser.add_argument("--nnps", dest="nnps",
            nargs='+',
            choices=['ll', 'sh', 'bs', 'esh', 'sym', 'sr', 'tree', 'comp_tree', 'ci',\
                'sfc', 'ckdtree', 'flann'],
            default=['ll', 'sh', 'esh', 'sym', 'sr', 'tree', 'ci', 'comp_tree', 'sfc', 'ckdtree', 'flann'],
            help="Use one of the linked list algorithm ('ll') or "\
                 "the spatial hash algorithm ('sh') or "\
                 "the extended spatial hash algorithm ('esh') or "\
                 "the extended spatial hash algorithm with sym mask('esh') or "\
                 "the stratified radius algorithm ('sr') or "\
                 "the octree algorithm ('tree') or "\
                 "the compressed octree algorithm ('comp_tree') or "\
                 "the cell indexing algorithm or "\
                 "the space filling curve or"\
                 "the scipy.spatial.cKDTree"\
                 "the NanoFLANN algorithm"
                )

    # General benchmarking options
    parser.add_argument("--all", dest="all",
            action="store_true", default=False)

    parser.add_argument("--dump-plots", dest="plot",
            action="store_true", default=False)

    parser.add_argument("--config", dest="cfg",
            default="config.txt", type=str)

    parser.add_argument("--plot-points", dest="plot_points",
            action="store_true", default=False)

    options = parser.parse_args()

    benchmarks = get_benchmarks()

    if options.plot_dataset:
        inst = Benchmark()

        datasets = [getattr(inst, name) for name in dir(inst)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")

        inst.plot_mayavi(datasets[test], 10, options)

    if not options.all:
        if len(options.benchmarks) == 0:
            print_benchmarks(benchmarks)
            choices = raw_input("Choose benchmarks: ")
            chosen_benchmarks = [benchmarks[int(i)] for i in choices.split()]
        else:
            chosen_benchmarks = [benchmarks[i] for i in options.benchmarks]
    else:
        choices = range(len(benchmarks))
        chosen_benchmarks = [benchmarks[i] for i in choices]

    run_benchmarks(chosen_benchmarks, options)

