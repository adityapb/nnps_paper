#!usr/bin/env python
import numpy
import numpy as np
import time
import json
import os
import pandas as pd
from pysph.base.utils import get_particle_array
from pyzoltan.core.carray import UIntArray
import math

def load_config(filename):
    with open(filename, 'r') as input_file:
        return json.load(input_file)

class PlotData(object):
    def __init__(self, nnps, config, x_label, y_label, tag = ""):
        self.x_axis = []
        self.radii = []
        self.hmin = []
        self.hmax = []
        self.y_axis = {nps: [] for nps in nnps}

        self.x_label = x_label
        self.y_label = y_label
        self.tag = tag
        self.cfg = config

        self.nnps = nnps

        self.plots = {nps: None for nps in nnps}

class Benchmark(object):
    def __init__(self):
        pass

    def plot_from_csv(self, filename):
        dataframe = pd.DataFrame.from_csv(filename)
        cfg = "config.txt"
        labels, colors = load_config(cfg)
        nnps = []
        for sym, name in labels.iteritems():
            if name in list(dataframe):
                nnps.append(sym)

        data = PlotData(nnps, cfg, "$N$", "Time")
        data.hmin = list(dataframe["h_min"])
        data.hmax = list(dataframe["h_max"])
        data.x_axis = list(dataframe["N"])
        data.y_axis = {nps : list(dataframe[labels[nps]]) for nps in nnps}
        self.dump_plot(filename.split(".")[0], data)

    def _benchmark_nnps(self, options, test):
        pa_lengths = range(options.min_length, options.max_length)
        data = PlotData(options.nnps, options.cfg, "$N$", "Time")
        prev_num_particles = 0

        i = 0
        num_tests_done = 0
        while num_tests_done != len(pa_lengths):
            pa = test(pa_lengths[0] + i, options)
            i += 1

            if pa.get_number_of_particles() == prev_num_particles:
                continue
            num_tests_done += 1
            prev_num_particles = pa.get_number_of_particles()
            h = (float(options.min_length)**3/prev_num_particles)**(1/3.)*options.h
            h_max = (float(options.min_length)**3/prev_num_particles)**(1/3.)*options.h_max
            h_min = (float(options.min_length)**3/prev_num_particles)**(1/3.)*options.h_min

            data.x_axis.append(pa.get_number_of_particles())
            data.radii.append(h)
            data.hmax.append(h_max)
            data.hmin.append(h_min)
            self.run_nnps(options, data, pa)

        self.print_results(data)
        dataframe = self.get_dataframe(data)
        dataframe.to_csv(options.filename + '.csv')
        if options.plot:
            self.dump_plot(options, data)

    def make_random_radius(self, N, options):
        x, y, z = numpy.mgrid[0:1:N*1j, 0:1:N*1j, 0:1:N*1j]
        x = x.ravel()
        y = y.ravel()
        z = z.ravel()

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        h = h_min + (h_max - h_min)*numpy.random.random_sample(len(x))
        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_gradient_radius(self, N, options):
        x, y, z = numpy.mgrid[0:1:N*1j, 0:1:N*1j, 0:1:N*1j]
        x = x.ravel()
        y = y.ravel()
        z = z.ravel()

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        if len(options.param_N) != 0:
            h_min = (float(min(options.param_N))**3/N**3)**(1/3.)*options.h_min
            h_max = (float(min(options.param_N))**3/N**3)**(1/3.)*options.h_max
        if len(options.param_h_max) != 0:
            h_min = options.h_min
            h_max = options.h_max

        def get_h(i):
            return h_min + ((h_max - h_min)/(N-1))*i

        def get_dx(i):
            return (get_h(i)/(h_min*N))

        def get_length(i):
            return get_dx(i)*N

        x, y, z, h = numpy.array([]), numpy.array([]), numpy.array([]), numpy.array([])

        prev_length = 0
        for i in range(N):
            prev_length += get_dx(i)
            x_i, z_i = numpy.mgrid[-get_length(i)/2:get_length(i)/2:N*1j,
                    -get_length(i)/2:get_length(i)/2:N*1j]
            x_i = x_i.ravel()
            z_i = z_i.ravel()
            y_i = numpy.ones_like(x_i)*prev_length
            h_i = numpy.ones_like(x_i)*get_h(i)
            x = numpy.concatenate([x,x_i])
            y = numpy.concatenate([y,y_i])
            z = numpy.concatenate([z,z_i])
            h = numpy.concatenate([h,h_i])

        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_2d_gradient_radius(self, N, options):
        x, y, z = numpy.mgrid[0:1:N*1j, 0:1:N*1j, 0:1:N*1j]
        x = x.ravel()
        y = y.ravel()
        z = z.ravel()

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        if len(options.param_N) != 0:
            h_min = (float(min(options.param_N))**3/N**3)**(1/3.)*options.h_min
            h_max = (float(min(options.param_N))**3/N**3)**(1/3.)*options.h_max
        if len(options.param_h_max) != 0:
            h_min = options.h_min
            h_max = options.h_max

        def get_h(i):
            return h_min + ((h_max - h_min)/(N-1))*i

        def get_dx(i):
            return (get_h(i)/(h_min*N))

        def get_length(i):
            return get_dx(i)*N

        x, y, z, h = numpy.array([]), numpy.array([]), numpy.array([]), numpy.array([])

        prev_length = 0
        for i in range(N):
            prev_length += get_dx(i)
            x_i = numpy.mgrid[-get_length(i)/2:get_length(i)/2:N*1j]
            x_i = x_i.ravel()
            y_i = numpy.ones_like(x_i)*prev_length
            h_i = numpy.ones_like(x_i)*get_h(i)
            x = numpy.concatenate([x,x_i])
            y = numpy.concatenate([y,y_i])
            h = numpy.concatenate([h,h_i])

        return get_particle_array(x=x, y=y, h=h)

    def make_gaussian(self, N, options):
        x = options.std*numpy.random.randn(N**3)
        y = options.std*numpy.random.randn(N**3)
        z = options.std*numpy.random.randn(N**3)
        h = x**2 + y**2 + z**2
        h /= numpy.amax(h)
        h *= options.std

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        h = h_min + (h_max - h_min)*h
        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_section_radius(self, N, options):

        num_particles_per_dim_dense = (options.h_max/options.h_min)*N
        num_particles_per_dim_sparse = N - num_particles_per_dim_dense

        x, y, z = numpy.mgrid[0:0.5:(num_particles_per_dim_dense/2)*1j, \
                0:1:num_particles_per_dim_dense*1j, \
                0:1:num_particles_per_dim_dense*1j]
        x_dense = x.ravel()
        y_dense = y.ravel()
        z_dense = z.ravel()
        h_dense = numpy.ones_like(x_dense)

        x, y, z = numpy.mgrid[0.6:1.1:(num_particles_per_dim_sparse/2)*1j, \
                0:1:num_particles_per_dim_sparse*1j, \
                0:1:num_particles_per_dim_sparse*1j]
        x_sparse = x.ravel()
        y_sparse = y.ravel()
        z_sparse = z.ravel()
        h_sparse = numpy.ones_like(x_sparse)

        x = numpy.concatenate([x_sparse, x_dense])
        y = numpy.concatenate([y_sparse, y_dense])
        z = numpy.concatenate([z_sparse, z_dense])
        h = numpy.concatenate([h_sparse, h_dense])

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        h[:len(x_dense)] *= h_min
        h[len(x_dense):] *= h_max

        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_constant_radius(self, N, options):
        x, y, z = numpy.mgrid[0:1:N*1j, 0:1:N*1j, 0:1:N*1j]
        x = x.ravel()
        y = y.ravel()
        z = z.ravel()
        h = numpy.ones_like(x)
        h_current = (float(options.min_length)**3/len(x))**(1/3.)*options.h
        h *= h_current
        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_line_dataset(self, N, options):
        base = numpy.random.randn(N**3)
        e_x = 0.1*numpy.random.randn(N**3)
        e_y = 0.1*numpy.random.randn(N**3)
        e_z = 0.1*numpy.random.randn(N**3)

        x = base + e_x
        y = base + e_y
        z = base + e_z

        h_min = (float(options.min_length)**3/len(x))**(1/3.)*options.h_min
        h_max = (float(options.min_length)**3/len(x))**(1/3.)*options.h_max

        h = e_x**2 + e_y**2 + e_z**2
        h /= h.max()
        h = h_min + (h_max - h_min)*h

        h_current = (float(options.min_length)**3/len(x))**(1/3.)*options.h
        h *= h_current
        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_one_particle_far_away(self, N, options):
        x = options.std*numpy.random.randn(N**3).tolist()
        y = options.std*numpy.random.randn(N**3).tolist()
        z = options.std*numpy.random.randn(N**3).tolist()
        x.append(100)
        y.append(100)
        z.append(100)
        x = numpy.asarray(x)
        y = numpy.asarray(y)
        z = numpy.asarray(z)

        h = options.h_min + (options.h_max - options.h_min)*((x**2 + y**2 + z**2)**0.5)/173
        return get_particle_array(x=x, y=y, z=z, h=h)

    def make_1d_dataset(self, N, options):
        x = numpy.linspace(0, 1, num=N**3)
        h = numpy.ones_like(x)
        h_current = (float(options.min_length)**3/len(x))*options.h
        h *= h_current
        return get_particle_array(x=x, h=h)

    def benchmark_ll(self, options, pa):
        from pysph.base.nnps import LinkedListNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "LinkedListNNPS"
        start = time.time()
        nps = LinkedListNNPS(dim=3, particles=[pa], radius_scale=1, cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_bs(self, options, pa):
        from pysph.base.nnps import BoxSortNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "BoxSortNNPS"
        start = time.time()
        nps = BoxSortNNPS(dim=3, particles=[pa], radius_scale=1, cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_sh(self, options, pa):
        from pysph.base.nnps import SpatialHashNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "SpatialHashNNPS"
        start = time.time()
        nps = SpatialHashNNPS(dim=3, particles=[pa], radius_scale=1, cache=False)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_esh(self, options, pa):
        from pysph.base.nnps import ExtendedSpatialHashNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "ExtendedSpatialHashNNPS"
        start = time.time()
        nps = ExtendedSpatialHashNNPS(dim=3, particles=[pa], radius_scale=1,
                H=options.H, symmetric=False, cache=False)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_sym(self, options, pa):
        from pysph.base.nnps import ExtendedSpatialHashNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "ExtendedSpatialHashNNPS"
        start = time.time()
        nps = ExtendedSpatialHashNNPS(dim=3, particles=[pa], radius_scale=1,
                H=options.H, symmetric=True, cache=False)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_sr(self, options, pa):
        from pysph.base.nnps import StratifiedSFCNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "StratifiedSFCNNPS"
        start = time.time()
        nps = StratifiedSFCNNPS(dim=3, particles=[pa], radius_scale=1,
                num_levels=options.num_levels, cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_tree(self, options, pa):
        from pysph.base.nnps import OctreeNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "OctreeNNPS"
        start = time.time()
        nps = OctreeNNPS(dim=3, particles=[pa], radius_scale=1,
                leaf_max_particles=options.max_particles, cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_compressed_tree(self, options, pa):
        from pysph.base.nnps import CompressedOctreeNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "CompressedOctreeNNPS"
        start = time.time()
        nps = CompressedOctreeNNPS(dim=3, particles=[pa], radius_scale=1,
                leaf_max_particles=options.max_particles, cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_ci(self, options, pa):
        from pysph.base.nnps import CellIndexingNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "CellIndexingNNPS"
        start = time.time()
        nps = CellIndexingNNPS(dim=3, particles=[pa], radius_scale=1,
                cache=False)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_sfc(self, options, pa):
        from pysph.base.nnps import ZOrderNNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        indices = UIntArray()
        print "\t Running", "."*20, "ZOrderNNPS"
        start = time.time()
        nps = ZOrderNNPS(dim=3, particles=[pa], radius_scale=1,
                cache=False)
        nps.get_spatially_ordered_indices(0, indices)
        nps.spatially_order_particles(0)
        nps.set_context(0,0)
        for i in range(num_particles):
            idx = indices[i]
            nps.get_nearest_particles(0,0,idx,nbrs)
        end = time.time()
        return end - start

    def benchmark_nanoflann(self, options, pa):
        from pysph.base.nnps import NanoFLANNPS
        num_particles = pa.get_number_of_particles()
        nbrs = UIntArray()
        print "\t Running", "."*20, "NanoFLANNPS"
        start = time.time()
        nps = NanoFLANNPS(dim=3, particles=[pa], radius_scale=1,
                cache=False, min_elements=options.max_particles)
        nps.set_context(0,0)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs)
        end = time.time()
        return end - start

    def benchmark_ckdtree(self, options, pa):
        from scipy.spatial import cKDTree
        num_particles = pa.get_number_of_particles()
        datapoints = zip(pa.x, pa.y, pa.z)
        print "\t Running", "."*20, "scipy.spatial.cKDTree"
        start = time.time()
        nps = cKDTree(datapoints)
        for i in range(num_particles):
            nps.query_ball_point(datapoints[i], pa.h[i])
        end = time.time()
        return end - start


    def run_ll(self, options, data, pa):
        data.y_axis['ll'].append(self.benchmark_ll(options, pa))

    def run_bs(self, options, data, pa):
        data.y_axis['bs'].append(self.benchmark_bs(options, pa))

    def run_sh(self, options, data, pa):
        data.y_axis['sh'].append(self.benchmark_sh(options, pa))

    def run_esh(self, options, data, pa):
        data.y_axis['esh'].append(self.benchmark_esh(options, pa))

    def run_sym(self, options, data, pa):
        data.y_axis['sym'].append(self.benchmark_sym(options, pa))

    def run_sr(self, options, data, pa):
        data.y_axis['sr'].append(self.benchmark_sr(options, pa))

    def run_tree(self, options, data, pa):
        data.y_axis['tree'].append(self.benchmark_tree(options, pa))

    def run_comp_tree(self, options, data, pa):
        data.y_axis['comp_tree'].append(self.benchmark_compressed_tree(options, pa))

    def run_ci(self, options, data, pa):
        data.y_axis['ci'].append(self.benchmark_ci(options, pa))

    def run_sfc(self, options, data, pa):
        data.y_axis['sfc'].append(self.benchmark_sfc(options, pa))

    def run_nanoflann(self, options, data, pa):
        data.y_axis['flann'].append(self.benchmark_nanoflann(options, pa))

    def run_ckdtree(self, options, data, pa):
        data.y_axis['ckdtree'].append(self.benchmark_ckdtree(options, pa))


    def run_nnps(self, options, data, pa):
        if 'esh' in options.nnps:
            self.run_esh(options, data, pa)

        if 'sym' in options.nnps:
            self.run_sym(options, data, pa)

        if 'll' in options.nnps:
            self.run_ll(options, data, pa)

        if 'bs' in options.nnps:
            self.run_bs(options, data, pa)

        if 'sh' in options.nnps:
            self.run_sh(options, data, pa)

        if 'sr' in options.nnps:
            self.run_sr(options, data, pa)

        if 'tree' in options.nnps:
            self.run_tree(options, data, pa)

        if 'comp_tree' in options.nnps:
            self.run_comp_tree(options, data, pa)

        if 'ci' in options.nnps:
            self.run_ci(options, data, pa)

        if 'sfc' in options.nnps:
            self.run_sfc(options, data, pa)

        if 'flann' in options.nnps:
            self.run_nanoflann(options, data, pa)

        if 'ckdtree' in options.nnps:
            self.run_ckdtree(options, data, pa)

        print "\n",

    def get_dataframe(self, data):
        """Prints timings in tabular form"""
        labels, colors = load_config(data.cfg)

        rows = ["N", "h", "h_min", "h_max"]
        rows += [labels[nps] for nps in data.nnps]

        times = [data.x_axis, data.radii, data.hmin, data.hmax]
        times += [data.y_axis[nps] for nps in data.nnps]

        return pd.DataFrame(times, rows).transpose()

    def print_results(self, data):
        print self.get_dataframe(data)

    def plot_dataset(self, dataset, N, options):

        import matplotlib as mpl
        #mpl.use('Agg')
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        pa = dataset(N, options)
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.scatter(pa.x, pa.y, pa.z, marker='x')

        for a in (ax.w_xaxis, ax.w_yaxis, ax.w_zaxis):
            for t in a.get_ticklines()+a.get_ticklabels():
                t.set_visible(False)
            a.line.set_visible(False)
            a.pane.set_visible(False)

        if options.plot:
            try:
                plt.savefig("output/%s.eps" % dataset.__name__, format='eps', dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig("output/%s.eps" % dataset.__name__, format='eps', dpi=1200)

            plt.close()
        else:
            plt.show()

    def plot_mayavi(self, dataset, N, options):
        from mayavi import mlab
        pa = dataset(N, options)
        mlab.points3d(pa.x, pa.y, pa.z)
        mlab.show()

    def dump_plot(self, options, *args):
        """Dumps plot"""

        import matplotlib as mpl
        mpl.use('Agg')
        import matplotlib.pyplot as plt
        from matplotlib import rc

        rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
        rc('text', usetex=True)

        fig = plt.figure()
        ax = fig.add_subplot(111)

        for data in args:
            labels, colors = load_config(data.cfg)
            for i, nps in enumerate(data.nnps):
                data.plots[nps], = ax.plot(data.x_axis, data.y_axis[nps],
                        color=colors[nps], label=" ".join([labels[nps], data.tag]))

        data = args[0]
        k_min = 1 + int(data.x_axis[0]*(data.hmin[0]**3))
        k_max = 1 + int(data.x_axis[0]*(data.hmax[0]**3))

        ax.set_xlabel(args[0].x_label)
        ax.set_ylabel(args[0].y_label)

        ax.legend(loc="upper left")
        ax.set_title("Time vs $N$, $k_{\min} = %g$, $k_{\max} = %g$" % (k_min, k_max))

        if type(options) is not str:
            try:
                plt.savefig(options.filename + ".eps", format='eps', dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig(options.filename + ".eps", format='eps', dpi=1200)
        else:
            try:
                plt.savefig(options + ".eps", format='eps', dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig(options + ".eps", format='eps', dpi=1200)

        plt.close()

