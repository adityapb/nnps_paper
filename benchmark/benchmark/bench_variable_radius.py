#!/usr/bin/env python
from benchmark import Benchmark
import numpy as np
import cPickle as pickle
import os
from pysph.base.utils import get_particle_array
from pyzoltan.core.carray import UIntArray

class VariableRadiusBenchmark(Benchmark):
    def __init__(self):
        Benchmark.__init__(self)

    def time_all_nnps_random_radius(self, options):
        """NNPS for random radius"""
        self._benchmark_nnps(options, self.make_random_radius)

    def time_all_nnps_gradient_radius(self, options):
        """NNPS for gradient radius"""
        self._benchmark_nnps(options, self.make_gradient_radius)

    def time_all_nnps_section_radius(self, options):
        """NNPS for section radius"""
        self._benchmark_nnps(options, self.make_section_radius)

    def time_all_nnps_gaussian(self, options):
        """NNPS for gaussian"""
        self._benchmark_nnps(options, self.make_gaussian)

    def time_all_nnps_one_particle_far_away(self, options):
        """NNPS for one particle far away"""
        self._benchmark_nnps(options, self.make_one_particle_far_away)

    def time_all_nnps_1d_dataset(self, options):
        """NNPS for 1d dataset"""
        self._benchmark_nnps(options, self.make_1d_dataset)

    def time_all_nnps_line_dataset(self, options):
        """NNPS for line dataset"""
        self._benchmark_nnps(options, self.make_line_dataset)

class ConstantRadiusBenchmark(Benchmark):
    def __init__(self):
        Benchmark.__init__(self)

    def time_all_nnps_constant_radius(self, options):
        """NNPS for constant radius"""
        self._benchmark_nnps(options, self.make_constant_radius)

class PlotOctree(Benchmark):
    def __init__(self):
        Benchmark.__init__(self)

    def time_plot_octree_nnps_leaf_max_particles_const_N(self, options):
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc
        import time

        from pysph.base.nnps import OctreeNNPS

        fig = plt.figure()

        #choose dataset
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        tests = raw_input("Choose tests to plot: ").split()
        N = input("Number of particles (N^1/3): ")

        for test in tests:
            dataset = datasets[int(test)]
            pa = dataset(N, options)
            num_particles = pa.get_number_of_particles()

            leaf_max_particles_list = np.arange(2, 200, 10)
            times = []
            depths = []
            nbrs = UIntArray()

            for leaf_max_particles in leaf_max_particles_list:
                start = time.time()
                nps = OctreeNNPS(dim=3, particles=[pa], radius_scale=1,
                        leaf_max_particles=leaf_max_particles, cache=False)
                nps.set_context(0,0)
                for i in range(num_particles):
                    nps.get_nearest_particles(0,0,i,nbrs)
                end = time.time()
                times.append(end - start)
                #depths.append(nps.get_depth(0))

            times = np.array(times)
            times = times / np.min(times)
            #depths = np.array(depths, dtype=np.float64)
            #depths_scaled = np.max(times) * (depths / np.max(depths))
            plt.plot(leaf_max_particles_list, times, label=dataset.__name__)
            #plt.plot(leaf_max_particles_list, depths_scaled, label=dataset.__name__ + " depths")

        plt.legend()
        if options.plot:
            try:
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)
        else:
            plt.show()

    def time_plot_octree_nnps_leaf_max_particles_const_test(self, options):
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc
        import time

        from pysph.base.nnps import OctreeNNPS

        fig = plt.figure()

        #choose dataset
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")
        #N = input("Number of particles (N^1/3): ")

        for i in range(3):
            N = 30 + i*10
            dataset = datasets[test]
            pa = dataset(N, options)
            num_particles = pa.get_number_of_particles()

            leaf_max_particles_list = np.arange(2, 300, 5)
            times = []
            nbrs = UIntArray()

            for leaf_max_particles in leaf_max_particles_list:
                start = time.time()
                nps = OctreeNNPS(dim=3, particles=[pa], radius_scale=1,
                        leaf_max_particles=leaf_max_particles, cache=False)
                nps.set_context(0,0)
                for i in range(num_particles):
                    nps.get_nearest_particles(0,0,i,nbrs)
                end = time.time()
                times.append(end - start)

            plt.plot(leaf_max_particles_list, np.array(times))

        if options.plot:
            try:
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)
        else:
            plt.show()


    def time_plot_octree(self, options):
        """Plot Octree for a dataset"""

        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        from pysph.base.octree import Octree

        fig = plt.figure()
        ax = Axes3D(fig)

        #choose dataset
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")
        N = input("Number of particles (N^1/3): ")

        dataset = datasets[test]
        pa = dataset(N, options)

        tree = Octree(options.max_particles)
        tree.build_tree(pa)

        tree.plot(ax)

        for a in (ax.w_xaxis, ax.w_yaxis, ax.w_zaxis):
            for t in a.get_ticklines()+a.get_ticklabels():
                t.set_visible(False)
            a.line.set_visible(False)
            a.pane.set_visible(False)

        if options.plot_points:
            ax.scatter(pa.x, pa.y, zs=pa.z, marker="x")

        if options.plot:
            try:
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig("output/octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)

        else:
            plt.show()

    def time_plot_compressed_octree(self, options):
        """Plot CompressedOctree for a dataset"""

        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        from pysph.base.octree import CompressedOctree

        fig = plt.figure()
        ax = Axes3D(fig)

        #choose dataset
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")
        N = input("Number of particles (N^1/3): ")

        dataset = datasets[test]
        pa = dataset(N, options)

        tree = CompressedOctree(options.max_particles)
        tree.build_tree(pa)

        tree.plot(ax)

        for a in (ax.w_xaxis, ax.w_yaxis, ax.w_zaxis):
            for t in a.get_ticklines()+a.get_ticklabels():
                t.set_visible(False)
            a.line.set_visible(False)
            a.pane.set_visible(False)

        if options.plot_points:
            ax.scatter(pa.x, pa.y, zs=pa.z, marker="x")

        if options.plot:
            try:
                plt.savefig("output/comp_octree_%s.eps" % dataset.__name__, format='eps', dpi=1200)
            except:
                os.mkdir("output")
                plt.savefig("output/comp_octree_%s.eps" % dataset.__name__, format='eps',
                        dpi=1200)

        else:
            plt.show()

    def time_plot_octree_leaf_cells(self, options):
        """Plot leaf cells in Octree for a dataset"""

        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        from pysph.base.octree import Octree

        fig = plt.figure()
        ax = Axes3D(fig)

        #choose dataset
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")
        N = input("Number of particles (N^1/3): ")

        dataset = datasets[test]
        pa = dataset(N, options)

        tree = Octree(options.max_particles)
        tree.build_tree(pa)

        leaves = tree.get_leaf_cells()

        for leaf in leaves:
            leaf.plot(ax)

        if options.plot_points:
            ax.scatter(pa.x, pa.y, zs=pa.z, marker="x")

        plt.show()

class CheckAccuracy(Benchmark):
    def __init__(self):
        Benchmark.__init__(self)

    def time_check_accuracy_of_nanoflann(self, options):
        from pysph.base.nnps import OctreeNNPS
        from pysph.base.nnps import NanoFLANNPS
        datasets = [getattr(self, name) for name in dir(self)
                if name.startswith('make')]

        for i, func in enumerate(datasets):
            print i, func.__name__

        test = input("Choose test to plot: ")
        N = input("Number of particles (N^1/3): ")

        dataset = datasets[test]
        pa = dataset(N, options)

        nps = OctreeNNPS(dim=3, particles=[pa], radius_scale=1,
                leaf_max_particles=options.max_particles, cache=False)

        flann = NanoFLANNPS(dim=3, particles=[pa], radius_scale=1,
                cache=False, min_elements=options.min_particles)

        nbrs_nps = UIntArray()
        nbrs_flann = UIntArray()

        num_particles = pa.get_number_of_particles()

        accuracy = np.zeros(num_particles)
        for i in range(num_particles):
            nps.get_nearest_particles(0,0,i,nbrs_nps)
            nbrs_nps_np = nbrs_nps.get_npy_array()
            flann.get_nearest_particles(0,0,i,nbrs_flann)
            nbrs_flann_np = nbrs_flann.get_npy_array()
            accuracy[i] = len(np.intersect1d(nbrs_nps_np, nbrs_flann_np))/float(len(nbrs_nps_np))

        print "Average accuracy = %g" % np.average(accuracy)
        print "Minimum accuracy = %g" % accuracy.min()

class SearchOptimalParameter(Benchmark):
    def __init__(self):
        Benchmark.__init__(self)

    def time_plot_optimal_num_levels_with_varying_N(self, options):
        """Plot time vs num_levels with varying N"""

        import matplotlib as mpl
        mpl.use('Agg')
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

        fig = plt.figure()
        ax = fig.add_subplot(111)

        plot_data = np.empty([len(options.param_N), len(options.param_num_levels)])
        datapoints = []
        for i, n in enumerate(options.param_N):
            pa = self.make_gradient_radius(n, options)
            for j, num_levels in enumerate(options.param_num_levels):
                options.num_levels = num_levels
                plot_data[i][j] = self.benchmark_sr(options, pa)
            ax.plot(options.param_num_levels, plot_data[i],
                    "--o", color=np.random.rand(3,1), label="Number of particles = %i" % n**3)

        dump_data = {}
        dump_data["N"] = options.param_N
        dump_data["num_levels"] = options.param_num_levels
        dump_data["plot_data"] = plot_data

        ax.set_ylabel("Time")
        ax.set_xlabel("Number of levels")
        ax.legend(loc="upper right")
        ax.title("Time vs number of levels for h ratio = %g" % options.h_max/options.h_min)

        try:
            plt.savefig("output/plot_optimal_num_levels_N.eps", format='eps', dpi=600)
            pickle.dump(dump_data, open("output/plot_optimal_num_levels_N.dat", "wb"))
        except:
            os.mkdir("output")
            plt.savefig("output/plot_optimal_num_levels_N.eps", format='eps', dpi=600)
            pickle.dump(dump_data, open("output/plot_optimal_num_levels_N.dat", "wb"))

    def time_plot_optimal_num_levels_with_varying_h_ratio(self, options):
        """Plot time vs num_levels with varying h ratio"""

        import matplotlib as mpl
        mpl.use('Agg')
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        from matplotlib import rc

        #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

        fig = plt.figure()
        ax = fig.add_subplot(111)

        plot_data = np.empty([len(options.param_h_max), len(options.param_num_levels)])
        datapoints = []
        for i, h_max in enumerate(options.param_h_max):
            options.h_max = h_max
            pa = self.make_gradient_radius(options.N, options)
            for j, num_levels in enumerate(options.param_num_levels):
                options.num_levels = num_levels
                plot_data[i][j] = self.benchmark_sr(options, pa)
            ax.plot(options.param_num_levels, plot_data[i],
                    "--o", color=np.random.rand(3,1), label="h_max = %g" % h_max)

        dump_data = {}
        dump_data["h_max"] = options.param_h_max
        dump_data["num_levels"] = options.param_num_levels
        dump_data["plot_data"] = plot_data

        ax.set_ylabel("Time")
        ax.set_xlabel("Number of levels")
        ax.legend(loc="upper right")
        ax.set_title("Time vs number of levels for N = %i" % options.N**3)

        try:
            plt.savefig("output/plot_optimal_num_levels_h_max.eps", format='eps', dpi=600)
            pickle.dump(dump_data, open("output/plot_optimal_num_levels_h_max.dat", "wb"))
        except:
            os.mkdir("output")
            plt.savefig("output/plot_optimal_num_levels_h_max.eps", format='eps', dpi=600)
            pickle.dump(dump_data, open("output/plot_optimal_num_levels_h_max.dat", "wb"))


